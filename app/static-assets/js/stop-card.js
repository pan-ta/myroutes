import * as headerControls from "./modules/headerControls.js";
import {data} from "./modules/data.js";
import {checkStorageAndSetupView} from "./modules/checkStorageAndSetupView.js";


const currentStop = data.stops[0];
const currentRoutes = currentStop.routes.map((route) => {return ` ${route.routeName}`});
// details

const pageTitle = document.querySelector(".title");
const detailsFields = document.querySelectorAll(".details-section__string");

pageTitle.textContent = `Остановка "${currentStop.name}"`
detailsFields[0].children[1].textContent = currentStop.id;
detailsFields[1].children[1].textContent = currentStop.name;
detailsFields[2].children[1].textContent = currentRoutes;
detailsFields[3].children[1].innerHTML = `<a href="#" class="details-section__link" id="timetable-btn"><i class="far fa-calendar-alt"></i></a>`;


// stopsTable

//ya-maps

const referencePoint = [];
referencePoint.push(currentStop.coord1);
referencePoint.push(currentStop.coord2);




function init() {
    var myMap = new ymaps.Map("map", {
        center: [59.949281, 30.244350],
        zoom: 13,
        controls: ['smallMapDefaultSet']
        }, {
            searchControlProvider: 'yandex#search'
        });

    myMap.geoObjects
        .add(new ymaps.Placemark(referencePoint, {
            balloonContent: currentStop.name,
        }));
    }

ymaps.ready(init);

const storage = window.localStorage;
checkStorageAndSetupView(storage);