import * as headerControls from "./modules/headerControls.js";
import {checkStorageAndSetupView} from "./modules/checkStorageAndSetupView.js";
import {data} from "./modules/data.js";
import {deletetBtnHtml, editBtnHtml, addBtnHtml} from "./modules/editBtnsDOM.js";


const currentRoute = data.routes[0];
const stopsArr = data.stops;

const stopsNames = [];

function getStopsNames (route) {
    route.stops.forEach(routeStop => {
        stopsArr.forEach(stop => {
            if (routeStop === stop.id)
            {
                stopsNames.push(stop.name);
                
            }
        })
    });
}

getStopsNames(currentRoute);

// details

const pageTitle = document.querySelector(".title");
const detailsFields = document.querySelectorAll(".details-section__string");

pageTitle.textContent = `Маршрут №${currentRoute.name}`;
detailsFields[0].children[1].textContent = currentRoute.transportType;
detailsFields[1].children[1].textContent = stopsNames[0];
detailsFields[2].children[1].textContent = stopsNames[stopsNames.length-1];
detailsFields[3].children[1].textContent = stopsNames.length;


// stopsTable

const routeTable = document.querySelector(".table");

function renderOneParamTable (table, dataArr) {
    dataArr.forEach((elem, i) => {
        const newRow = table.insertRow();
        newRow.classList.add("table-data-row", "table-row");
        const newNmbCell = newRow.insertCell();
        newNmbCell.textContent = i+1;
        newNmbCell.classList.add("table-data-cell", "table-cell");
        const newDataCell = newRow.insertCell();
        newDataCell.textContent = elem;
        newDataCell.classList.add("table-data-cell", "table-cell");
    })
}

renderOneParamTable(routeTable, stopsNames);

//ya-maps

const referencePointsArr = [];

function getReferencePoints (route) {
    route.stops.forEach(routeStop => {
        stopsArr.forEach(stop => {
            if (routeStop === stop.id)
            {
                let coords = [];
                coords.push(stop.coord1);
                coords.push(stop.coord2);
                referencePointsArr.push(coords);
                coords = [];
            }
        })
    });
}

getReferencePoints(currentRoute);



function init () {
    /**
     * Создаем мультимаршрут.
     * Первым аргументом передаем модель либо объект описания модели.
     * Вторым аргументом передаем опции отображения мультимаршрута.
     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRoute.xml
     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/multiRouter.MultiRouteModel.xml
     */
    var multiRoute = new ymaps.multiRouter.MultiRoute({
        // Описание опорных точек мультимаршрута.
        referencePoints: referencePointsArr,
        // Параметры маршрутизации.
        params: {
            // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
            results: 1
        }
    }, {
        // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
        boundsAutoApply: true,
         
    });

    // Создаем карту с добавленными на нее кнопками.
    var myMap = new ymaps.Map('map', {
        center: [59.942327, 30.275925],
        zoom: 10,
        controls: ['smallMapDefaultSet']
    }, {

    });

    // Добавляем мультимаршрут на карту.
    myMap.geoObjects.add(multiRoute);
}

ymaps.ready(init);





const storage = window.localStorage;
checkStorageAndSetupView(storage);