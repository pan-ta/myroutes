import * as headerControls from "./modules/headerControls.js";
import {data} from "./modules/data.js";
import {showElem, hideElem, isVisible, hasClass, getElem, getElems, addDOMElement} from "./modules/baseFuncsDOM.js";
import {renderTable} from "./modules/renderTable.js";
import sortAndRender from "./modules/sortAndRender.js";
import listenToInputAndRender from "./modules/listenToInputAndRender.js";
import {convertSortBtnToAsc, convertSortBtnToDesc, convertSortBtnToInit} from "./modules/sortBtnView.js";
import {checkStorageAndSetupView} from "./modules/checkStorageAndSetupView.js";



const thisData = data.stops;


const thisTable = getElem(document, ".table");
const thisTableParams = {
    columns: 3,
    contentFuncs: [
        data => data.id,
        data => data.name,
        data => data.routes.map((route) => {return ` ${route.routeName}`}),
    ],
    rowClass: "table-data-row",
    cellClass: "table-cell",
    firstCellLink: "/stop-card"
}


renderTable(thisTable, thisData, thisTableParams);


//////////////////// sort

const idSortBtn = getElem(thisTable, "#id-sort-btn");
const sortBtns = getElems(thisTable, ".sort-btn");

idSortBtn.addEventListener("click", () => {

    if (hasClass(idSortBtn, "sorted-asc")) {
        sortAndRender(false, "id", thisData, thisTable, thisTableParams);
        convertSortBtnToDesc(idSortBtn);

    } else {
        sortAndRender(true, "id", thisData, thisTable, thisTableParams);
        convertSortBtnToAsc(idSortBtn);
    }
});


/////////////////////filter

const idFilterBtn = getElem(thisTable, "#id-filter-btn");
const idFilterBlock = getElem(thisTable, "#id-filter-block");
const idFilterInput = getElem(thisTable, "#id-filter-input");
const filterBlocks = getElems(thisTable, ".filter-block");

idFilterBtn.addEventListener("click", () => {
    //cancel sort and re-render
    sortBtns.forEach(btn => {
        if (hasClass(btn, "sorted-asc") || hasClass(btn, "sorted-desc")) {
            convertSortBtnToInit(btn);
            renderTable(thisTable, thisData, thisTableParams);
        }
    })
    
    //close other filters and re-render
    filterBlocks.forEach(block => {
        if (isVisible(block)) {
            hideElem(block);
            renderTable(thisTable, thisData, thisTableParams);
        }
    });

    showElem(idFilterBlock);

    //filter
    idFilterInput.oninput = () => {
        listenToInputAndRender("id", idFilterInput.value, thisTable, thisData, thisTableParams);
    };

    //close 
    const closeBtn = getElem(idFilterBlock, ".close-btn");
    closeBtn.addEventListener("click", () => {
        hideElem(idFilterBlock);
        renderTable(thisTable, thisData, thisTableParams);
    })
    
})


const storage = window.localStorage;
checkStorageAndSetupView(storage);


