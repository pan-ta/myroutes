import * as headerControls from "./modules/headerControls.js";
import {checkStorageAndSetupView} from "./modules/checkStorageAndSetupView.js";


const storage = window.localStorage;
checkStorageAndSetupView(storage);
