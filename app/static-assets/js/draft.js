var data = {
    "stops": [
        {
            "id": 12345,
            "coord1": 59.951887, 
            "coord2": 30.237281,
            "name": "переулок Каховского",
            "routes": [
                {
                    "routeId": 10040,
                    "timeTable": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeId": 11040,
                    "timeTable": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
            
        },
        {
            "id": 12346,
            "coord1": 59.951166, 
            "coord2": 30.246701,
            "name": "проспект КИМа",
            "routes": [
                {
                    "routeId": 10040,
                    "timeTable": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeId": 11040,
                    "timeTable": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 12347,
            "coord1": 59.947668, 
            "coord2": 30.255692,
            "name": "набережная реки Смоленки",
            "routes": [
                {
                    "routeId": 10040,
                    "timeTable": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeId": 11040,
                    "timeTable": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        }
    ],
    "routes": [
        {
            "id": 10040,
            "name": "40",
            "stops": [12345, 12346, 12347],
            "tranports": [106789, 106790, 106791, 1067892],
            "transportType": "bus"
        },
        {
            "id": 11040,
            "name": "40R",
            "stops": [12347, 12346, 12345],
            "tranports": [106789, 106790, 106791, 1067892],
            "transportType": "bus"
        }
    ],
    "transports": [
        {
            "id": 106789,
            "transportType": "bus",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106790,
            "transportType": "bus",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106791,
            "transportType": "bus",
            "routeId": 10040,
            "seats": 120
        },
        {
            "id": 106792,
            "transportType": "bus",
            "routeId": 10040,
            "seats": 120
        }
    ]
};

console.log(data.stops[1].routes[1].timeTable);

var texts = {
    "pages": [
        {
            "page": "index",
            "pageTitle": "myRoutes - построй свой маршрут!",
            "pageDesc": "myRoutes - информационный портал транспортной компании НовАвтоТранс. Мы располагаем автопарком, включающим 4 категории ТС, общим числом свыше 150 единиц транспорта. На данный момент НовАвтоТранс обслуживает коммерческие маршруты Васильевского острова. Испозуйте myRoutes, чтобы найти информацию об интересных Вам маршрутах и остановках, а также чтобы построить маршрут."
        },
        {
            "page": "route-card",
            "pageTitle": "Маршрут №",
            "pageDesc": "Здесь Вы можете посмотреть необходимую информацию о маршруте и увидеть маршрут на карте. Для построения маршрута перейдите сюда."
        }
    ]
};



Handlebars.registerHelper ('pageTitle', function() {
    var pageTitle = texts.pages[1].pageTitle;
    return pageTitle;
})


{/* <script>
    var window._Routes_App
    $(function(app) {
        app = {
            state : {
                index : {

                },
                routes : {

                },
                transport : {

                }
            }
        }
    })(_Routes_App);
</script> */}


/////////////// MVC

const app = {
    model: {
        trains: [],
        currentTrain: null,
    },
    view: {},
    controller: {},
};
window.onload = () => app.controller.init();
// CONTROLLER
app.controller.init = () => {
    app.controller.loadTrains();
};
app.controller.loadTrains = () => {
    makeGetXHR(TRAINS_URL, (err, trains) => {
        console.log('loadTrains', trains);
        app.model.trains = trains.departures.all || [];
        app.view.showTrainList();
    });
};
app.controller.loadTrainInfo = (id) => {
    const url = TRAIN_INFO_URL.replace('{{id}}', id);
    makeGetXHR(url, (err, trainInfo) => {
        app.model.currentTrain = trainInfo || [];
        app.view.showTrainInfo(trainInfo);
    });
};
// VIEW
app.view.showTrainList = ()=> {
    const trains = app.model.trains;
    const trainList = document.getElementById('trainList');
    trainList.innerHTML = '';
    trains.forEach(train => app.view.showTrain(train));
};
app.view.showTrain = (train) => {
    console.log('showTrain', train);
    const trainList = document.getElementById('trainList');
    const trainDiv = createDiv('train');
    const trainTime = createDiv('train__time',train.aimed_departure_time);
    const trainOrigin = createDiv('train__origin',train.origin_name);
    const trainDest = createDiv('train__destination',train.destination_name);
    const trainInfo = createDiv('train__button', 'Details');
    trainInfo.dataset.trainId = train.train_uid;
    trainInfo.addEventListener('click', (event) => {
        const trainId = event.target.dataset.trainId;
        app.controller.loadTrainInfo(trainId)
    });
    trainDiv.appendChild(trainTime);
    trainDiv.appendChild(trainOrigin);
    trainDiv.appendChild(trainDest);
    trainDiv.appendChild(trainInfo);
    trainList.appendChild(trainDiv)
};
app.view.showTrainInfo = (trainInfo) => {
    console.time('trainInfo');
    const trainInfoDiv = document.getElementById('trainInfo');
    trainInfoDiv.innerHTML = '';
    const trainOrigin = createDiv('train-info__origin',`Departure: ${trainInfo.origin_name}`);
    const trainDest = createDiv('train-info__destination',`Destination: ${trainInfo.destination_name}`);
    const trainTime = createDiv('train-info__departure-time',`Time: ${trainInfo.aimed_departure_time}`);
    const trainStops = createDiv('train-info__stops',`Stops`);
    trainInfoDiv.appendChild(trainOrigin);
    trainInfoDiv.appendChild(trainDest);
    trainInfoDiv.appendChild(trainTime);
    trainInfoDiv.appendChild(trainStops);
    app.view.showTrainStops(trainInfo);
    console.timeEnd('trainInfo');
};
app.view.showTrainStops = (trainInfo) => {
    console.log('showTrainStops', trainInfo);
    trainInfo.stops.forEach(stop => app.view.showStopDetails(stop));
};
app.view.showStopDetails = (trainStop) => {
    console.log('showStopDetails', trainStop);
    const stopsDiv = document.getElementsByClassName('train-info__stops')[0];
    const theStop = document.createElement('div');
    theStop.classList.add('train-stop');
    const time = createDiv('train-stop__time', `${trainStop.aimed_arrival_time} - ${trainStop.aimed_departure_time}`);
    const name = createDiv('train-stop__name', `${trainStop.station_name}`);
    stopsDiv.appendChild(theStop);
    theStop.appendChild(time);
    theStop.appendChild(name);
}

// const tableHeaderCells = document.querySelectorAll(".table-header-cell");

function addListenersToHeaders () {
    tableHeaderCells.forEach((cell)=> {
        cell.addEventListener("click", (e) => {
            if(e.target.parentElement) {
                if (e.target.parentElement.classList.contains("filter-btn")) {
                    cell.querySelector(".filter-block").classList.remove("disabled");tableHeaderCells.forEach((element)=> {
                        if(element.contains(e.target)==false) {
                            console.log("!!!");
                            console.log(e.target);
                            console.log(element);
                            renderTable(transportTable, transportData, renderParams.transportTable);
                            if(element.querySelector(".filter-block")) {
                                element.querySelector(".filter-block__input").value="";
                                element.querySelector(".filter-block").classList.add("disabled");
                            }
                        }
                    })
                }
                if (e.target.parentElement.classList.contains("filter-block__close-btn")) {
                    if (cell.querySelector(".filter-block__input")) {
                        cell.querySelector(".filter-block__input").value="";
                        renderTable(transportTable, transportData, renderParams.transportTable);
                        cell.querySelector(".filter-block").classList.add("disabled");
                    } 
                    else if (cell.querySelector(".filter-block__checkboxes")) {
                        renderTable(transportTable, transportData, renderParams.transportTable);
                        cell.querySelector(".filter-block").classList.add("disabled");
                    }
                }
            }
        })
    })
} 

addListenersToHeaders();