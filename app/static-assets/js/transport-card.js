import * as headerControls from "./modules/headerControls.js";
import {data} from "./modules/data.js";
import {checkStorageAndSetupView} from "./modules/checkStorageAndSetupView.js";



const currentTransport = data.transports[0];

// details

const pageTitle = document.querySelector(".title");
const detailsFields = document.querySelectorAll(".details-section__string");

pageTitle.textContent = `${currentTransport.transportType} №${currentTransport.id}`
detailsFields[0].children[1].textContent = currentTransport.id;
detailsFields[1].children[1].textContent = currentTransport.transportType;
detailsFields[2].children[1].textContent = currentTransport.routeName;
detailsFields[3].children[1].textContent = currentTransport.seats;



const storage = window.localStorage;
checkStorageAndSetupView(storage);