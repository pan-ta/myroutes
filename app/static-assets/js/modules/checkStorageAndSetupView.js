
function checkStorageAndSetupView(storage) {
    if (storage.editMode == "true") {
        console.log("edit mode");
        turnOnEditMode();
    }
}

function turnOnEditMode() {
    const allEditFeatures = document.querySelectorAll(".edit");
    allEditFeatures.forEach((feature) => {
        feature.classList.remove("disabled");
    } )
}

export {checkStorageAndSetupView};