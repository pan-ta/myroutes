import {renderTable} from "./renderTable.js";

export default function sortAndRender(isAsc, prop, data, table, tableParams) {

    function comparePropsAsc(a, b) {
        if (a[prop] > b[prop]) return 1;
        if (a[prop] < b[prop]) return -1;
    }
    function comparePropsDesc(a, b) {
        if (a[prop] > b[prop]) return -1;
        if (a[prop] < b[prop]) return 1;
    }
    let sortedData;

    if (isAsc) {
        sortedData = data.sort(comparePropsAsc);
    } else {
        sortedData = data.sort(comparePropsDesc);
    }

    renderTable (table, sortedData, tableParams);

}
