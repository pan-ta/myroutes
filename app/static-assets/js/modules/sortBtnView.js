const SortBtnAscInnerHTML = `<i class="fas fa-sort-up"></i>`;
const SortBtnDescInnerHTML = `<i class="fas fa-sort-down"></i>`;
const SortBtnInitInnerHTML = `<i class="fas fa-sort"></i>`;

function convertSortBtnToAsc(sortBtn) {
    sortBtn.innerHTML = SortBtnAscInnerHTML;
    console.log(sortBtn);
    sortBtn.classList.add("sorted-asc");
    sortBtn.classList.remove("sorted-desc");
}

function convertSortBtnToDesc(sortBtn) {
    sortBtn.innerHTML = SortBtnDescInnerHTML;
    sortBtn.classList.add("sorted-desc");
    sortBtn.classList.remove("sorted-asc");
}

function convertSortBtnToInit(sortBtn) {
    sortBtn.innerHTML = SortBtnDescInnerHTML;
    sortBtn.classList.remove("sorted-desc");
    sortBtn.classList.remove("sorted-asc");
}

export {convertSortBtnToAsc, convertSortBtnToDesc, convertSortBtnToInit};
