const deletetBtnHtml = `<button class="delete-btn edit disabled"><i class="fas fa-trash-alt"></i></button>`
const editBtnHtml = `<button class="edit-btn edit disabled"><i class="fas fa-edit"></i></button>`
const addBtnHtml = `<button class="add-btn edit disabled"><i class="fas fa-plus-square"></i></button>`

export {deletetBtnHtml, editBtnHtml, addBtnHtml};
