export const data = {
    "stops": [
        {
            "id": 12345,
            "coord1": 59.951887, 
            "coord2": 30.237281,
            "name": "пер. Каховского, 3",
            "routes": [
                {
                    "routeName": "40",
                    "timeTable": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "40R",
                    "timeTable": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
            
        },
        {
            "id": 12346,
            "coord1": 59.951166, 
            "coord2": 30.246701,
            "name": "пр. КИМа, 9",
            "routes": [
                {
                    "routeName": "40",
                    "timeTable": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "40R",
                    "timeTable": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        },
        {
            "id": 12347,
            "coord1": 59.947668, 
            "coord2": 30.255692,
            "name": "наб. реки Смоленки, 33",
            "routes": [
                {
                    "routeName": "40",
                    "timeTable": ["6:30", "6:45", "7:00", "7:15", "7:30", "7:45", "8:00"]
                },
                {
                    "routeName": "40R",
                    "timeTable": ["6:35", "6:50", "7:05", "7:20", "7:35", "7:50", "8:05"]
                }
            ]
        }
    ],
    "routes": [
        {
            "id": 10040,
            "name": "40",
            "stops": [12347, 12346, 12345],
            "transports": [106789, 106790, 106791, 106792],
            "transportType": "Автобус",
            "firstStop": "наб. реки Смоленки, 33",
            "lastStop": "пер. Каховского, 3"
        },
        {
            "id": 11040,
            "name": "40R",
            "stops": [12345, 12346, 12347],
            "transports": [106789, 106790, 106791, 106792],
            "transportType": "Автобус",
            "firstStop": "пер. Каховского, 3",
            "lastStop": "наб. реки Смоленки, 33"
        }
    ],
    "transports": [
        {
            "id": 106789,
            "transportType": "Автобус",
            "routeId": 10040,
            "routeName": 40,
            "seats": 120
        },
        {
            "id": 106790,
            "transportType": "Автобус",
            "routeId": 10040,
            "routeName": 40,
            "seats": 120
        },
        {
            "id": 106791,
            "transportType": "Автобус",
            "routeId": 10040,
            "routeName": 40,
            "seats": 120
        },
        {
            "id": 106792,
            "transportType": "Автобус",
            "routeId": 10040,
            "routeName": 40,
            "seats": 80
        },
        {
            "id": 116792,
            "transportType": "Маршрутное такси",
            "routeId": 11325,
            "routeName": 325,
            "seats": 40
        }
    ]
};

