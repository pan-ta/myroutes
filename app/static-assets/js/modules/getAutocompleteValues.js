export default function getAutocompleteValues(inputValue, allValues) {
    const autocompleteValues = [];
    allValues.forEach(value => {
        if (value.toString().substr(0, inputValue.length).toUpperCase() === inputValue.toUpperCase()) {
            autocompleteValues.push(value);
        }
    });
    return autocompleteValues;
}