import {showElem, hideElem, getElem, getElems, addDOMElement} from "./baseFuncsDOM.js";
import {deletetBtnHtml, editBtnHtml, addBtnHtml} from "./editBtnsDOM.js";

function renderTable (table, dataArr, tableParams) {
    while (table.contains(getElem(table, ".table-data-row"))) {
        getElem(table, ".table-data-row").remove();
    };

    dataArr.forEach(dataElem => {
        //get cell contents from data acc to params
        const cellContents = [];
        for (let i = 0; i<tableParams.columns; i++) {
            cellContents.push(tableParams.contentFuncs[i](dataElem));
        }

        createRow(table, tableParams.rowClass, tableParams.columns, tableParams.cellClass, cellContents, tableParams.firstCellLink);
    });
}

function createRow (table, className, columns, cellClassName, cellContent, cellLink) {
    const newRow = table.insertRow();
    newRow.classList.add(className);
    for (let i = 0; i<columns; i++) {
        if (i===0) {
            createLinkCell(newRow, cellClassName, cellContent[i], cellLink);
        } else {
            createCell(newRow, cellClassName, cellContent[i]);
        }
    }
    
}

function createCell (row, className, content) {
    const newCell = row.insertCell();
    newCell.textContent = content;
    newCell.classList.add(className);
}

function createLinkCell (row, className, content, cellLink) {
    const newCell = row.insertCell();
    newCell.innerHTML = `<a href="${cellLink}" class="table-link">${content}</a> ${editBtnHtml} ${deletetBtnHtml}`;
    newCell.classList.add(className);
}



export {renderTable};