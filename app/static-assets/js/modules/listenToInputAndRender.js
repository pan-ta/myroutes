import {renderTable} from "./renderTable.js";
import filterArrayByProp from "./filterArrayByProp.js";
import getAutocompleteValues from "./getAutocompleteValues.js";
import getPropValuesArr from "./getPropValuesArr.js";
import {showElem, hideElem, getElem, getElems, addDOMElement} from "./baseFuncsDOM.js";

export default function listenToInputAndRender (prop, inputValue, table, dataArr, tableParams) {

    const autocomplete = getElem(table, "#id-autocomplete");

    if(inputValue) {  
        //live search
        const newDataArr = filterArrayByProp(dataArr, prop, inputValue);
        renderTable(table, newDataArr, tableParams);

        //autocomplete
        const autocompleteList = getElem(autocomplete, "ul");
        const propValues = getPropValuesArr(dataArr, prop);
        const suggestedValues = getAutocompleteValues(inputValue, propValues);

        if (suggestedValues.length) {
            showElem(autocomplete);
            while (autocompleteList.contains(getElem(autocompleteList, "li"))) {
                getElem(autocompleteList, "li").remove();
            }
            suggestedValues.forEach(value => {
                addDOMElement(autocompleteList, "li", "autocomplete__item", value);
            })
        } else {
            while (autocompleteList.contains(getElem(autocompleteList, "li"))) {
                getElem(autocompleteList, "li").remove();
            }
            hideElem(autocomplete);
        }

    } else {
        hideElem(autocomplete);
        renderTable(table, dataArr, tableParams);
    }

}

