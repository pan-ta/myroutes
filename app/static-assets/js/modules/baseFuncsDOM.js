function showElem (elem) {
    elem.classList.remove("disabled");
}

function hideElem (elem) {
    elem.classList.add("disabled");
}

function isVisible (elem) {
    return !elem.classList.contains("disabled"); //??
}

function hasClass (elem, className) {
    return elem.classList.contains(className);
}

function getElem (where, selector) {
    return where.querySelector(selector);
}

function getElems (where, selector) {
    return where.querySelectorAll(selector);
}

function addDOMElement (parent, tag, className, textContent) {
    const newElem = document.createElement(tag);
    newElem.classList.add(className);
    if (textContent) {
        newElem.textContent = textContent;
    }
    parent.appendChild(newElem);
}

export {showElem, hideElem, isVisible, hasClass, getElem, getElems, addDOMElement};

