const profileBtn = document.getElementById("profile-btn");
const profileDropdown = document.getElementById("profile-dropdown");
const hamburgerBtn = document.getElementById("hamburger-btn");
const hamburgerDropdown = document.getElementById("hamburger-dropdown");
const logoutBtn = document.getElementById("logout-btn");


logoutBtn.addEventListener("click", () => {
    document.location.href = "/auth";
})


function toggleVisibility (elem) {
    elem.classList.toggle("disabled");
}

profileBtn.addEventListener("click", (e) => {
    e.preventDefault();
    toggleVisibility(profileDropdown);
})

hamburgerBtn.addEventListener("click", (e) => {
    e.preventDefault();
    toggleVisibility(hamburgerDropdown);
})


document.addEventListener("click", function(e) {

    hideOpenPopup(profileDropdown, profileBtn);
    hideOpenPopup(hamburgerDropdown, hamburgerBtn);

    function hideOpenPopup (popup, btn) {
        if(popup.classList.contains("disabled")==false) {
            if((popup !== e.target)&&(popup.contains(e.target)===false)&&(btn.firstElementChild !== e.target)) {
                popup.classList.add("disabled");
            };
        };
    }
});
