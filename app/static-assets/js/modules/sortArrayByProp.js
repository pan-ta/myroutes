export default function filterArrayByProp(arr, prop, value) {
    const filteredArr = arr.filter(element => {
        const comparedValue = element[prop].toString().toUpperCase();
        return comparedValue.includes(value.toUpperCase()); // возвращает true/false
    })
    return filteredArr;
}

function sortArrayByProp(arr, prop) {
    const sortedArr = arr.sort(element => {
        const comparedValue = element[prop].toString().toUpperCase();
        return comparedValue.includes(value.toUpperCase()); // возвращает true/false
    })
    return filteredArr;
}