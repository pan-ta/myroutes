export default function getPropValuesArr(objArr, prop) {
    const propValuesArr = [];
    objArr.forEach(obj => {
        propValuesArr.push(obj[prop]);
    });
    return propValuesArr;
}