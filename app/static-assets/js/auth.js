import {userData} from "./modules/userData.js";



console.log("!!");

const registrationBlock = document.getElementById("registration");
const authBlock = document.getElementById("auth");
const notRegisteredBtn = document.getElementById("not-registered");
const forgetPassLink = document.getElementById("forget-pass");
const restorePassBlock = document.getElementById("restore-pass");



notRegisteredBtn.addEventListener("click", (e) => {
    e.preventDefault();
    toggleVisibility(registrationBlock);
    toggleVisibility(authBlock);
})

forgetPassLink.addEventListener("click", (e) => {
    e.preventDefault();
    toggleVisibility(restorePassBlock);
    toggleVisibility(authBlock);
})

function toggleVisibility (elem) {
    elem.classList.toggle("disabled");
}



///////////////////
const mainContainer = document.getElementById("main-container");
const picLinks = ["/static assets/img/main.jpg", "/static-assets/img/02.jpg", "/static-assets/img/05.jpg", "/static-assets/img/06.jpg", "/static-assets/img/07.jpg", "/static-assets/img/main.jpg"];



function slideShow (container, linksArr, delay) {
    linksArr.forEach(function(link, i, arr) {
        setTimeout(function() {
            container.setAttribute("background-image", `"url(${link})"`);
            container.style.backgroundImage = `url(${link})`;
        }, delay*i);
    });
}

slideShow(mainContainer, picLinks, 8000);



//////////////////

const storage = window.localStorage;
storage.setItem("editMode", false);
console.log("edit mode canceled");


const authForm = document.getElementById("auth-form");

authForm.onsubmit = () => {
    // e.preventDefault();
    
    if (authForm.login.value === userData.admins[0].login) {
        console.log("admin in, edit mode on");
        storage.editMode = true;
        console.log(storage.editMode);
    } 
    document.location.href = "/index";
    return false;
}



const registerSubmitBtn = document.querySelector("#register-submit-btn");
registerSubmitBtn.addEventListener("click", (e) => {
    e.preventDefault();
    document.location.href = "/index";
})

const restorePassSubmitBtn = document.querySelector("#restore-pass-submit-btn");
restorePassSubmitBtn.addEventListener("click", (e) => {
    e.preventDefault();
    document.location.href = "/index";
})